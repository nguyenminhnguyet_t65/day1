<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>day07</title>
    <style>
    /* Định dạng chung cho các phần tử form */
    .form-input {
        display: flex;
        align-items: center;
    }

    /* Định dạng cho phần "Phân khoa" và "Từ khoá" */
    .input-label {
        width: 150px; /* Độ rộng của label */
        padding: 15px;
        color: black;
        margin-left: 500px; 
        
    }

    .input-field {
        border: 2px solid rgb(48, 113, 178);
        padding: 10px;
        width: 100%;
    }

    /* Định dạng cho nút "Tìm kiếm" */
    .search-button {
        padding: 10px 20px;
        border-radius: 5px;
        background-color: rgb(102 153 204);
        color: white;
        cursor: pointer;
        margin: 0 auto; /* Để nút tìm kiếm ở giữa */
        display: block; /* Để nút chiếm toàn bộ chiều rộng của phần tử cha */
    }

    .search-button:hover {
        background-color: rgb(102 153 204);
    }

    

    .keyword-input {
        border: 2px solid rgb(48, 113, 178);
        padding: 8px;
        width: 50%;
        margin-left: 10px; /* Tạo khoảng cách với label "Từ khoá" */
    }

    button:hover {
        background-color: rgb(24, 87, 182);
    }

    /* Định dạng cho bảng */
    .table {
        width: 100%;
        border-collapse: collapse;
    }

    /* Định dạng cho cột "No" */
    .table th:nth-child(1) {
        width: 50px;
    }

    /* Định dạng cho cột "Tên sinh viên" */
    .table th:nth-child(2) {
        width: 250px;
    }

    /* Định dạng cho cột "Khoa" */
    .table th:nth-child(3) {
        width: 200px;
    }

    /* Định dạng cho cột "Action" */
    .table th:nth-child(4) {
        width: 100px;
    }

    /* Định dạng cho các hàng trong bảng */
    .table td {
        
        padding: 10px;
        text-align: center;
    }

    .add-button {
        padding: 10px 20px;
        border-radius: 5px;
        background-color: rgb(102 153 204);
        color: white;
        cursor: pointer;
        float: right; /* Đặt nút "Thêm" bên phải */
        margin-right: 100px; /* Tạo khoảng cách với phần bên phải */
    }

    .add-button:hover {
        background-color: rgb(24, 87, 182);
    }

    .edit-button, .delete-button {
        padding: 5px 10px;
        border: none;
        border-radius: 5px;
        text-decoration: none;
        color: white;
        background-color: rgb(102 153 204); /* Màu xanh dương nhạt */
        margin-right: 5px;
    }

    .keyword-input {
        border: 2px solid rgb(48, 113, 178);
        padding: 8px;
        width: 100%;
        margin-left: 0px; /* Tạo khoảng cách với label "Từ khoá" */
    }

</style>

</head>
<body>
    <div class="d-flex form-input mb-20">
        <div class="w-100 input-label">Phân khoa</div>
        <div class="w-100">
            <select class="input-field" name="department" id="department" onchange="searchTable()">
                <?php
                $departments = array(
                    array('id' => '0', 'name' => '-- Chọn phân khoa --'),
                    array('id' => 'MAT', 'name' => 'Khoa học máy tính'),
                    array('id' => 'KDL', 'name' => 'Khoa học vật liệu')
                );

                foreach ($departments as $department) {
                    echo '<option value="' . $department['id'] . '">' . $department['name'] . '</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="d-flex form-input mb-20">
        <div class="w-100 input-label">Từ khoá</div>
        <div class="w-100">
            <input type="text" id="searchInput" class="keyword-input" oninput="searchTable()">
        </div>
    </div>

    <div class="text-center">
        <button onclick="searchTable()" class="search-button">Tìm kiếm</button>
    </div>

    <div id="searchCount">Số sinh viên tìm thấy: </div>

    <script>
        document.getElementById('searchInput').addEventListener('input', searchTable);
        document.getElementById('department').addEventListener('change', searchTable);
    </script>

    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">
        <a href="register2.php" class="add-button">Thêm</a>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Tên sinh viên</th>
                <th scope="col">Khoa</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include 'database.php';
            $sql = "SELECT * FROM students";
            $result = mysqli_query($conn, $sql);

            if ($result && mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '<tr>';
                    echo '<td>' . $row['id'] . '</td>';
                    echo '<td>' . $row['hovaten'] . '</td>';
                    echo '<td>' . $row['phankhoa'] . '</td>';
                    echo '<td>';
                    echo '<a href="edit.php?id=' . $row['id'] . '" class="edit-button">Sửa</a>';
                    echo '<a href="delete.php?id=' . $row['id'] . '" class="delete-button">Xoá</a>';
                    echo '</td>';
                    echo '</tr>';
                }
            } else {
                echo "Không có dữ liệu hoặc có lỗi trong truy vấn.";
            }
            ?>
        </tbody>
    </table>
</body>
</html>
