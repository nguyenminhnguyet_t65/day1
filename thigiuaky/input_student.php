<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>bai3</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        form {
            width: 550px;
            padding: 60px 40px;
        }

        .d-flex {
            display: flex;
        }

        .p-20 {
            padding: 10px;
        }

        .p-8 {
            padding: 8px;
        }

        .m-20 {
            margin: 20px;
        }

        .me-20 {
            margin-right: 20px;
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-100 {
            width: 100%;
        }

        .form-input input {
            padding: 10px 0 11px 10px;
        }

        .form-input input:hover {
            border-color: rgb(102 153 204);
        }

        .bg-blue {
            background-color: rgb(103, 163, 93);
            width: 130px;
            height: 5.5vh;
        }

        .bgblue {
            background-color: rgb(102 153 204);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(48 113 178);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 13px 45px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
        }

        .btn:hover {
            background-color: rgb(24 87 182);
        }

        .txt {
            width: 100%;
            padding: 8px;
            border: 1px solid lightcoral;
        }

        .loi {
            width: 100%;
            padding: 6px;
            border: 2px solid red;
        }

        #baoloi {
            color: red;
        }

        .do {
            color: red;
            margin-left: 5px;
        }
    </style>
</head>

<body>
    <form action="confirm.php" method="POST" enctype="multipart/form-data" class="bd-blue" onsubmit="return kiemtra()">
        <p><label></label> <span id="baoloi"></span></p>

        <script>
            function kiemtra() {
                var loi = "";
                // kiểm tra họ và tên: textbox
                var hoten = document.getElementById("hovaten");
                if (hoten.value == "") {
                    hoten.className = "loi";
                    loi += "Hãy nhập tên.<br>";
                }

                // kiểm tra phân khoa: select
                var phankhoa = document.getElementById("department");
                if (phankhoa.value == 0) {
                    phankhoa.className = "loi";
                    loi += "Hãy chọn phân khoa.<br>";
                }

                // kiểm tra ngày sinh
                var ngaySinh = document.getElementById("ngaySinh").value;
                var thangSinh = document.getElementById("thangSinh").value;
                var namSinh = document.getElementById("namSinh").value;

                if (ngaySinh == "0" || thangSinh == "0" || namSinh == "0") {
                    ngaySinh.className = "loi";
                    thangSinh.className = "loi";
                    namSinh.className = "loi";
                    loi += "Hãy nhập ngày sinh đầy đủ.<br>";
                }

                // Kiểm tra định dạng ngày sinh
                var ngaysinhPattern = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/;
                if (!ngaysinhPattern.test(ngaySinh + '/' + thangSinh + '/' + namSinh)) {
                    ngaySinh.className = "loi";
                    thangSinh.className = "loi";
                    namSinh.className = "loi";
                    loi += "Hãy nhập ngày sinh đúng định dạng (dd/mm/yyyy).<br>";
                }

                // kiểm tra Thành Phố và Quận
                var thanhPho = document.getElementById("thanhPho");
                var quanHuyen = document.getElementById("quanHuyen");

                if (thanhPho.value == "0") {
                    thanhPho.className = "loi";
                    loi += "Hãy chọn thành phố.<br>";
                }

                if (thanhPho.value == "HaNoi") {
                    var quanOptions = ["-- Chọn quận/huyện --", "Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                    if (quanOptions.indexOf(quanHuyen.value) === -1) {
                        quanHuyen.className = "loi";
                        loi += "Hãy chọn quận/huyện của Hà Nội.<br>";
                    }
                } else if (thanhPho.value == "TPHCM") {
                    var quanOptions = ["-- Chọn quận/huyện --", "Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                    if (quanOptions.indexOf(quanHuyen.value) === -1) {
                        quanHuyen.className = "loi";
                        loi += "Hãy chọn quận/huyện của TP.Hồ Chí Minh.<br>";
                    }
                }

                // trả về giá trị kiểm tra
                if (loi != "") {
                    document.getElementById('baoloi').innerHTML = "<p>" + loi + "</p>";
                    return false;
                }
            }
        </script>

        <div class="d-flex form-input mb-20">
            <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Họ và tên<span class="do">*</span></div>
            <div class="w-100">
                <input class="w-100 bd-blue" type="text" id="hovaten" name="hovaten">
            </div>
        </div>

        <div class="d-flex form-input mb-20">
            <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Giới tính<span class="do">*</span></div>
            <div class="w-100" id="gioitinh" name="gioitinh">
                <input type="radio" id="nam" name="gender" value="Nam" />
                <label for="nam">Nam</label>
                <input type="radio" id="nu" name="gender" value="Nữ" />
                <label for="nu">Nữ</label>
            </div>
        </div>

        <div class="d-flex form-input mb-20">
            <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Ngày sinh<span class="do">*</span></div>
            <div class="w-100">
                <select class="bd-blue department-input" name="ngaySinh" id="ngaySinh">
                    <option value="0">Ngày</option>
                    <?php
                    // Thêm tùy chọn cho ngày từ 01 đến 31
                    for ($i = 1; $i <= 31; $i++) {
                        $day = str_pad($i, 2, '0', STR_PAD_LEFT);
                        echo '<option value="' . $day . '">' . $day . '</option>';
                    }
                    ?>
                </select>
                <select class="bd-blue department-input" name="thangSinh" id="thangSinh">
                    <option value="0">Tháng</option>
                    <?php
                    // Thêm tùy chọn cho tháng từ 01 đến 12
                    for ($i = 1; $i <= 12; $i++) {
                        $month = str_pad($i, 2, '0', STR_PAD_LEFT);
                        echo '<option value="' . $month . '">' . $month . '</option>';
                    }
                    ?>
                </select>
                <select class="bd-blue department-input" name="namSinh" id="namSinh">
                    <option value="0">Năm</option>
                    <?php
                    // Chọn năm hiện tại - 40 đến năm hiện tại - 15
                    $currentYear = date("Y");
                    for ($i = $currentYear - 40; $i >= $currentYear - 15; $i--) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

        <!-- Thành Phố -->
<div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Thành Phố<span class="do">*</span></div>
    <div class="w-100">
        <select class="bd-blue department-input" name="thanhPho" id="thanhPho" onchange="updateQuanHuyen()">
            <option value="0">-- Chọn thành phố --</option>
            <option value="HaNoi">Hà Nội</option>
            <option value="TPHCM">Tp.Hồ Chí Minh</option>
        </select>
    </div>
</div>

<!-- Quận/Huyện -->
<div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Quận/Huyện<span class="do">*</span></div>
    <div class="w-100">
        <select class="bd-blue department-input" name="quanHuyen" id="quanHuyen">
            <option value="0">-- Chọn quận/huyện --</option>
        </select>
    </div>
</div>

<script>
    // Hàm để cập nhật danh sách quận tùy theo thành phố
    function updateQuanHuyen() {
        var thanhPho = document.getElementById("thanhPho");
        var quanHuyen = document.getElementById("quanHuyen");
        
        // Xóa tất cả các tùy chọn hiện tại
        quanHuyen.innerHTML = "";

        // Tạo một danh sách tùy chọn dựa vào lựa chọn của thành phố
        if (thanhPho.value === "HaNoi") {
            var quanOptions = ["-- Chọn quận/huyện --", "Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
        } else if (thanhPho.value === "TPHCM") {
            var quanOptions = ["-- Chọn quận/huyện --", "Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
        } else {
            // Thành phố khác, có thể cung cấp danh sách quận/huyện tương ứng ở đây
            var quanOptions = ["-- Chọn quận/huyện --"];
        }

        // Thêm các tùy chọn vào phần "Quận/Huyện"
        for (var i = 0; i < quanOptions.length; i++) {
            var option = document.createElement("option");
            option.value = quanOptions[i];
            option.text = quanOptions[i];
            quanHuyen.appendChild(option);
        }
    }
</script>


        <div class="d-flex form-input1 mb-20">
            <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Thông tin khác</div>
            <div class="w-100">
                <textarea class="w-100 bd-blue" name="thongTinKhac"></textarea>
            </div>
        </div>

    </div>

    <div class="text-center ">
        <button type="submit" class="btn bd-blue bgblue text-white">Đăng ký</button>
    </div>

    </form>
</body>

</html>
