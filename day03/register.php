<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>bai3</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }


        form {
            width: 550px;
            padding: 60px 40px;
        }


        .d-flex {
            display: flex;
        }   

        .p-20 {
            padding: 10px;
        }

        .p-8{
            padding: 8px;
        }

        .m-20 {
            margin: 20px;
        }

        .me-20 {
            margin-right: 20px
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-100 {
            width: 100%;
        }

        .form-input input {
            padding: 10px 0 11px 10px;
        }

        .form-input1 input {
            padding: 70px 0 10px 10px;
        }
        .form-input input:hover {
            border-color: rgb(102 153 204);
        }

        .bg-blue {
            background-color: rgb(102 153 204);
            width: 130px;
            height: 5.5vh;
        }
        .bgblue{
            background-color: rgb(102 153 204);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(48 113 178);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 13px 45px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
        }

        .btn:hover {
            background-color: rgb(24 87 182);
        }

        .txt { 
            width: 100%; padding: 8px; border: 1px solid lightcoral; 
        }

        .loi { 
            width: 100%; padding: 6px; border:2px solid red; 
        }
        #baoloi { 
            color: red; 
        }

        .do {
            color: red;
            margin-left: 5px;
        }

    </style>

</head>

<body>
    <form action="" class="bd-blue" >

    <div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Họ và tên<span class="do">*</span></div>
    <div class="w-100">
        <input class="w-100 bd-blue" type="text" id="hovaten" name="hovaten" >
    </div>
</div>
    <div class="d-flex form-input mb-20">
        <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Giới tính<span class="do">*</span></div>
        <div class="w-100" id="gioitinh">
            <?php
            $genders = array(
                array('id' => '0', 'name' => 'Nam'),
                array('id' => '1', 'name' => 'Nữ')
            );

            for ($i = 0; $i < count($genders); $i++) {
                echo '<input type="radio" id="' . $genders[$i]['id'] . '" name="gender" value="' . $genders[$i]['name'] . '" />';
                echo '<label for="' . $genders[$i]['id'] . '">' . $genders[$i]['name'] . '</label>';
            }
            ?>
        </div>
    </div>

    <div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Phân khoa<span class="do">*</span></div>
    <div class="w-100">
        <select class="bd-blue department-input" name="department" id="department">
            <?php
            $departments = array(
                array('id' => '0', 'name' => '-- Chọn phân khoa --'),
                array('id' => 'MAT', 'name' => 'Khoa học máy tính'),
                array('id' => 'KDL', 'name' => 'Khoa học vật liệu')
            );

            foreach ($departments as $department) {
                echo '<option value="' . $department['id'] . '">' . $department['name'] . '</option>';
            }
            ?>
        </select>
    </div>
</div>

    <div class="text-center ">
        <button type="submit" class="btn bd-blue bgblue text-white">Đăng ký</button>
    </div>

    </form>
</body>
</html>