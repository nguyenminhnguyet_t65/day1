<?php
include 'database.php';
if (isset($_GET['id'])) {
    
    $studentId = mysqli_real_escape_string($conn, $_GET['id']);
    $deleteQuery = "DELETE FROM students WHERE id = '$studentId'";

    if (mysqli_query($conn, $deleteQuery)) {
        header('Location: depkay.php');
        exit();
    } else {
        echo "Lỗi xóa bản ghi: " . mysqli_error($conn);
    }
} else {
    header('Location: depkay.php');
    exit();
}
mysqli_close($conn);
?>
