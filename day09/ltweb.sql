-- Tạo cơ sở dữ liệu ltweb
CREATE DATABASE IF NOT EXISTS ltweb;

-- Sử dụng cơ sở dữ liệu ltweb
USE ltweb;

-- Tạo bảng students để lưu thông tin đăng ký sinh viên
CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    hovaten VARCHAR(255) NOT NULL,
    gioitinh ENUM('Nam', 'Nữ') NOT NULL,
    phankhoa VARCHAR(255) NOT NULL,
    ngaysinh VARCHAR(255) NOT NULL,
    diachi VARCHAR(255),
    hinhanh BLOB
);
