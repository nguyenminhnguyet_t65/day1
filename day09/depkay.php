<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>day07</title>
    <style>
    
    .form-input {
        display: flex;
        align-items: center;
    }

    .input-label {
        width: 150px; 
        padding: 15px;
        color: black;
        margin-left: 500px; 
        
    }

    .input-field {
        border: 2px solid rgb(48, 113, 178);
        padding: 10px;
        width: 100%;
    }

    .search-button {
        padding: 10px 20px;
        border-radius: 5px;
        background-color: rgb(102 153 204);
        color: white;
        cursor: pointer;
        margin: 0 auto; 
        display: block; 
    }

    .search-button:hover {
        background-color: rgb(102 153 204);
    }


    .keyword-input {
        border: 2px solid rgb(48, 113, 178);
        padding: 8px;
        width: 50%;
        margin-left: 10px;
    }

    button:hover {
        background-color: rgb(24, 87, 182);
    }

    .table {
        width: 100%;
        border-collapse: collapse;
    }

    .table th:nth-child(1) {
        width: 50px;
    }

    .table th:nth-child(2) {
        width: 250px;
    }

    .table th:nth-child(3) {
        width: 200px;
    }

    .table th:nth-child(4) {
        width: 100px;
    }

    .table td {
        
        padding: 10px;
        text-align: center;
    }

    .add-button {
        padding: 10px 20px;
        border-radius: 5px;
        background-color: rgb(102 153 204);
        color: white;
        cursor: pointer;
        float: right; 
        margin-right: 100px;
    }

    .add-button:hover {
        background-color: rgb(24, 87, 182);
    }

    .edit-button, .delete-button {
        padding: 5px 10px;
        border: none;
        border-radius: 5px;
        text-decoration: none;
        color: white;
        background-color: rgb(102 153 204); 
        margin-right: 5px;
    }

    .keyword-input {
        border: 2px solid rgb(48, 113, 178);
        padding: 8px;
        width: 100%;
        margin-left: 0px; 
    }

</style>

</head>
<body>
<div class="d-flex form-input mb-20">
    <div class="w-100 input-label">Phân khoa</div>
    <div class="w-100">
        <select class="input-field" name="department" id="department">
                <?php
                $departments = array(
                    array('id' => '0', 'name' => '-- Chọn phân khoa --'),
                    array('id' => 'MAT', 'name' => 'Khoa học máy tính'),
                    array('id' => 'KDL', 'name' => 'Khoa học vật liệu')
                );

                foreach ($departments as $department) {
                    echo '<option value="' . $department['id'] . '">' . $department['name'] . '</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="d-flex form-input mb-20">
    <div class="w-100 input-label">Từ khoá</div>
    <div class="w-100">
        <input type="text" id="searchInput" class="keyword-input">
    </div>
    </div>

    <div class="text-center">
        <button onclick="searchTable" class="search-button">Tìm kiếm</button>
    </div>
</div>

    

<div id="searchCount">Số sinh viên tìm thấy: XXX</div>

<script>
    function searchTable() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById('searchInput');
        filter = input.value.toUpperCase();
        table = document.querySelector('.table');
        tr = table.getElementsByTagName('tr');
        var count = 0;

        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName('td')[1]; 
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = '';
                    count++; 
                } else {
                    tr[i].style.display = 'none';
                }
            }
        }

        document.getElementById('searchCount').textContent = 'Số sinh viên tìm thấy: ' + count;
    }
</script>

    
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">
        <a href="register2.php" class="add-button">Thêm</a>
    </div>
    
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Tên sinh viên</th>
                <th scope="col">Khoa</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
    
    <?php
    // Kết nối cơ sở dữ liệu
    include 'database.php';

    $sql = "SELECT * FROM students";
    $result = mysqli_query($conn, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['id'] . '</td>';
            echo '<td>' . $row['hovaten'] . '</td>';
            echo '<td>' . $row['phankhoa'] . '</td>';
            echo '<td>';
            echo '<a href="update_student.php?id=' . $row['id'] . '" class="edit-button">Sửa</a>';
            echo '<a href="delete.php?id=' . $row['id'] . '" class="delete-button">Xoá</a>';
            echo '</td>';
            echo '</tr>';
        }
    } else {
        echo "Không có dữ liệu hoặc có lỗi trong truy vấn.";
    }
    ?>

    </tbody>
    </table>

    <script>
    // Hàm hiển thị hộp thoại xác nhận
    function showConfirmationPopup(studentId) {
        var isConfirmed = confirm("Bạn muốn xóa sinh viên này?");
        if (isConfirmed) {
            window.location.href = "delete.php?id=" + studentId;
        }
    }

    var deleteButtons = document.getElementsByClassName("delete-button");
    for (var i = 0; i < deleteButtons.length; i++) {
        deleteButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var studentId = this.getAttribute('href').split('=')[1];
            
            showConfirmationPopup(studentId);
        });
    }
</script>


</body>
</html>
