<?php
// Thông tin kết nối đến cơ sở dữ liệu
$dbHost = 'localhost'; // Thay đổi nếu cần
$dbUser = 'root'; // Thay đổi nếu cần
$dbPassword = ''; // Thay đổi nếu cần
$dbName = 'ltweb';

// Kết nối đến cơ sở dữ liệu
$conn = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);

// Kiểm tra kết nối
if (!$conn->connect_error) {
    die("Lỗi kết nối đến cơ sở dữ liệu: " . $conn->connect_error);
}
echo "Kết nối thành công ";
?>
