<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>day07</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }


        form {
            width: 550px;
            padding: 60px 40px;
        }


        .d-flex {
            display: flex;
        }   

        .p-20 {
            padding: 10px;
        }

        .p-8{
            padding: 8px;
        }

        .m-20 {
            margin: 20px;
        }

        .me-20 {
            margin-right: 20px
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-100 {
            width: 100%;
        }

        .form-input input {
            padding: 10px 0 11px 10px;
        }

        .form-input1 input {
            padding: 70px 0 10px 10px;
        }
        .form-input input:hover {
            border-color: rgb(102 153 204);
        }

        .bg-blue {
            background-color: rgb(103, 163, 93);
            width: 130px;
            height: 5.5vh;
        }
        .bgblue{
            background-color: rgb(102 153 204);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(48 113 178);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 13px 45px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
        }

        .btn:hover {
            background-color: rgb(24 87 182);
        }

        .txt { 
            width: 100%; padding: 8px; border: 1px solid lightcoral; 
        }

        .loi { 
            width: 100%; padding: 6px; border:2px solid red; 
        }
        #baoloi { 
            color: red; 
        }

        .do {
            color: red;
            margin-left: 5px;
        }

    </style>

</head>

<body>

<div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Họ và tên<span class="do">*</span></div>

/body>
</html>
