<?php
// Thông tin kết nối đến cơ sở dữ liệu
$dbHost = 'localhost'; // Thay đổi nếu cần
$dbUser = 'root'; // Thay đổi nếu cần
$dbPassword = ''; // Thay đổi nếu cần
$dbName = 'ltweb';

// Kết nối đến cơ sở dữ liệu
$conn = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);

// Kiểm tra kết nối
if (!$conn->connect_error) {
    die("Lỗi kết nối đến cơ sở dữ liệu: " . $conn->connect_error);
}
echo "Kết nối thành công ";


// Add a student
if ($_POST['action'] == 'add_student') {
    // Retrieve form data
    $name = $_POST['hovaten'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthdate = $_POST['ngaysinh'];
    $address = $_POST['diachi'];
    // Perform the INSERT SQL query here
}

// Edit a student
if ($_POST['action'] == 'edit_student') {
    // Retrieve form data and student ID
    $student_id = $_POST['student_id'];
    $name = $_POST['hovaten'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthdate = $_POST['ngaysinh'];
    $address = $_POST['diachi'];
    // Perform the UPDATE SQL query here
}

// Delete a student
if ($_POST['action'] == 'delete_student') {
    // Retrieve student ID and perform the DELETE SQL query here
}

// Fetch and display student records
$result = $conn->query("SELECT * FROM students");
if ($result->num_rows > 0) {
    // Display student records in a table
}
$conn->close();
?>

?>
