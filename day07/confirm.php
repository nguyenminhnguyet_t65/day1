<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận thông tin đăng ký</title>

    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }


        form {
            width: 550px;
            padding: 60px 40px;
        }


        .d-flex {
            display: flex;
        }   

        .p-20 {
            padding: 10px;
        }

        .p-8{
            padding: 8px;
        }

        .m-20 {
            margin: 20px;
        }

        .me-20 {
            margin-right: 20px
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-100 {
            width: 100%;
        }

        .form-input input {
            padding: 10px 0 11px 10px;
        }

        .form-input1 input {
            padding: 70px 0 10px 10px;
        }
        .form-input input:hover {
            border-color: rgb(102 153 204);
        }

        .bg-blue {
            background-color: rgb(103, 163, 93);
            width: 130px;
            height: 5.5vh;
        }
        .bgblue{
            background-color: rgb(102 153 204);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(48 113 178);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 13px 45px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
        }

        .btn:hover {
            background-color: rgb(24 87 182);
        }

        .hinh {
            flex: 2;
            padding: 8px;
            float: left; 
            margin-right: 175px;
            
        }

    </style>
</head>
<body>
    <form action="" class="bd-blue" >

   <div class="d-flex form-input mb-20">
   <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Họ và tên</div>
   <div class="w-100">
       <?php
       if (isset($_POST['hovaten'])) {
        echo htmlspecialchars($_POST['hovaten']);
       }
       ?>
   </div>
</div>

    <div class="d-flex form-input mb-20">
        <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Giới tính</div>
        <div class="w-100" id="gioitinh">
            <?php
            if (isset($_POST['gender'])) {
                echo htmlspecialchars($_POST['gender']);
            }
            ?>
        </div>
    </div>

    <div class="d-flex form-input mb-20">
        <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Phân khoa</div>
        <div class="w-100">
        <?php
        if (isset($_POST['department'])) {
            $selectedDepartmentId = $_POST['department'];

            $departments = array(
                array('id' => '0', 'name' => '-- Chọn phân khoa --'),
                array('id' => 'MAT', 'name' => 'Khoa học máy tính'),
                array('id' => 'KDL', 'name' => 'Khoa học vật liệu')
            );

            // Tìm tên phân khoa tương ứng với id đã chọn
            $selectedDepartmentName = '-- Không xác định --'; // Giá trị mặc định

            foreach ($departments as $department) {
                if ($department['id'] === $selectedDepartmentId) {
                    $selectedDepartmentName = $department['name'];
                    break;
                }
            }

            echo htmlspecialchars($selectedDepartmentName);
        }
        ?>
        </div>
    </div>
    
    <div class="d-flex form-input mb-20">
        <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Ngày sinh</div>
        <div class="w-100">
        <?php
            if (isset($_POST['ngaysinh'])) {
                    echo htmlspecialchars($_POST['ngaysinh']);
            }
            ?>
        </div>
    </div>

    <div class="d-flex form-input mb-20">
    <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Địa chỉ</div>
    <div class="w-100">
        <?php
            if (isset($_POST['diachi'])) {
                echo htmlspecialchars($_POST['diachi']);
            }
        ?>
    </div>
    </div>  

    <div class="d-flex form-input1 mb-20">
        <div class="w-100 p-8 bg-blue text-white me-20 bd-blue">Hình ảnh</div>
        <div class="w-100">
            <?php
            if (isset($_FILES['hinhanh'])) {
                $hinhanhName = $_FILES['hinhanh']['name'];
                $hinhanhTmpName = $_FILES['hinhanh']['tmp_name'];
                $hinhanhSize = $_FILES['hinhanh']['size'];
            
                // Đảm bảo thư mục "uploads" tồn tại trước khi lưu hình ảnh
                if (!is_dir('uploads')) {
                    mkdir('uploads', 0755, true);
                }
            
                // Xử lý và lưu trữ hình ảnh
                $uploadPath = 'uploads/' . $hinhanhName;
                
                if (move_uploaded_file($hinhanhTmpName, $uploadPath)) {
                    echo '<img src="' . $uploadPath . '" alt="Hình ảnh" style = "max-width: 140px;" class = "hinh">';
                }
            }
            ?>
        </div>
    </div>
    </div>

    <?php
    // Include tệp kết nối cơ sở dữ liệu
include 'database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy dữ liệu từ form
    $hovaten = $_POST['hovaten'];
    $gioitinh = $_POST['gender'];
    $phankhoa = $_POST['department'];
    $ngaysinh = $_POST['ngaysinh'];
    $diachi = $_POST['diachi'];
    $hinhanh = $_FILES['hinhanh']['name'];

    // Thực hiện truy vấn để chèn dữ liệu vào bảng "students"
    $sql = "INSERT INTO students (hovaten, gioitinh, phankhoa, ngaysinh, diachi, hinhanh)
            VALUES ('$hovaten', '$gioitinh', '$phankhoa', '$ngaysinh', '$diachi', '$hinhanh')";

    if (mysqli_query($conn, $sql)) {
        // Thành công
        echo "Dữ liệu đã được lưu vào cơ sở dữ liệu.";
    } else {
        // Lỗi
        echo "Lỗi: " . $sql . "<br>" . mysqli_error($conn);
    }
}
?>


    <div class="text-center ">
        <button type="submit" class="btn bd-blue bgblue text-white">Xác nhận</button>
    </div>

</form>
    
</body>
</html>
