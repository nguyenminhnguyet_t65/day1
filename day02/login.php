<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>Document</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }


        form {
            width: 650px;
            padding: 40px 80px;
        }


        .d-flex {
            display: flex;
        }   

        .p-20 {
            padding: 20px;
        }

        .p-8{
            padding: 8px;
        }

        .m-20 {
            margin: 20px;
        }

        .me-20 {
            margin-right: 20px
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .w-50 {
            width: 50%;
        }

        .w-100 {
            width: 100%;
        }

        .present-time {
            background-color: rgb(229 229 229);
            margin:0 0 20px 0;
        }

        .form-input input {
            padding: 20px 0 20px 10px;
        }

        .form-input input:hover {
            border-color: rgb(102 153 204);
        }

        .bg-blue {
            background-color: rgb(102 153 204);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(48 113 178);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 15px 50px;
            border-radius: 10px;
            cursor: pointer;
        }

        .btn:hover {
            background-color: rgb(24 87 182);
        }
    </style>
</head>

<body>
    <form action="" class="bd-blue">
    <?php
            date_default_timezone_set('Asia/Ho_Chi_Minh'); // Đặt múi giờ Việt Nam
            $today = date("H:i"); // Lấy giờ:phút:giây hiện tại
            $dayOfWeek = date("N"); // Lấy số thứ trong tuần (1: Thứ 2, 2: Thứ 3, ..., 7: Chủ Nhật)
            $date = date("d/m/Y"); // Lấy ngày/tháng/năm hiện tại
            
            // Mảng ánh xạ các giá trị từ tiếng Anh sang tiếng Việt
            $daysInVietnamese = array(
                1 => 'Thứ 2',
                2 => 'Thứ 3',
                3 => 'Thứ 4',
                4 => 'Thứ 5',
                5 => 'Thứ 6',
                6 => 'Thứ 7',
                7 => 'Chủ Nhật',
            );

            $dayOfWeekVietnamese = $daysInVietnamese[$dayOfWeek]; // Lấy tên thứ tiếng Việt
            
            ?>
            <p class="present-time p-8">Bây giờ là
                <?php echo $today; ?>,
                <?php echo $dayOfWeekVietnamese; ?> ngày
                <?php echo $date; ?>
            </p>
        <div class="d-flex form-input mb-20">
            <div class="w-50 p-20 bg-blue text-white me-20 bd-blue">Tên đăng nhập</div>
            <div class="w-50 ">
                <input class="w-100 bd-blue" type="text">
            </div>
        </div>
        <div class="d-flex form-input pass-input">
            <div class="w-50 p-20 bg-blue text-white me-20 bd-blue">Mật khẩu</div>
            <div class="w-50 ">
                <input class="w-100 bd-blue" type="password">
            </div>
        </div>
        <div class="text-center ">
            <button type="submit" class="btn bd-blue bg-blue text-white">Đăng nhập</button>
        </div>

    </form>
</body>

</html>

